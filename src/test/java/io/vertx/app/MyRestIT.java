package io.vertx.app;

import static com.jayway.restassured.RestAssured.delete;
import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import static org.assertj.core.api.StrictAssertions.assertThat;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.jayway.restassured.RestAssured;

import io.vertx.model.ServiceHealth;

/**
 * These tests checks our REST API.
 */
public class MyRestIT {

	@BeforeClass
	public static void configureRestAssured() {
		RestAssured.baseURI = "http://localhost";
		RestAssured.port = Integer.getInteger("http.port", 8080);
	}

	@AfterClass
	public static void unconfigureRestAssured() {
		RestAssured.reset();
	}

	@Test
	public void testCompleteFlow() {
		ServiceHealth service = given().body("{\"id\":\"1\", \"name\":\"Google\", \"url\":\"https://www.google.com\"}")
				.request().post("/kry/service").thenReturn().as(ServiceHealth.class);
		assertThat(service.getName()).isEqualToIgnoringCase("Google");
		assertThat(service.getUrl()).isEqualToIgnoringCase("https://www.google.com");
		assertThat(service.getId()).isNotEmpty();

		get("/kry/service").then().assertThat().statusCode(200);

		delete("/kry/service/" + service.getId()).then().assertThat().statusCode(204);

	}
}
