package io.vertx.app;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.model.ServiceHealth;
import io.vertx.util.ServiceUtil;

public class ServiceHealthVerticle extends AbstractVerticle {
	private static final String SERVICE_URL = "/kry/service";
	private ServiceUtil serviceUtil;
	private static final Logger LOGGER = Logger.getLogger(ServiceHealthVerticle.class);

	@Override
	public void start(Future<Void> fut) {
		serviceUtil = new ServiceUtil();
		// Create a router object.
		Router router = Router.router(vertx);
		// Bind "/" to our hello message.
		router.route("/").handler(routingContext -> {
			HttpServerResponse response = routingContext.response();
			response.putHeader("content-type", "text/html").end("<h1>Hello service health application</h1>");
		});

		router.route("/services/*").handler(StaticHandler.create("services"));
		router.route(SERVICE_URL + "*").handler(BodyHandler.create());
		router.get(SERVICE_URL).handler(this::getAll);
		router.post(SERVICE_URL).handler(this::addOne);
		router.put(SERVICE_URL + "/:id").handler(this::updateOne);
		router.delete(SERVICE_URL + "/:id").handler(this::deleteOne);

		vertx.createHttpServer().requestHandler(router::accept).listen(
				// Retrieve the port from the configuration,
				// default to 8080.
				config().getInteger("http.port", 8080), result -> {
					if (result.succeeded()) {
						fut.complete();
					} else {
						fut.fail(result.cause());
					}
				});
	}

	private void getAll(RoutingContext routingContext) {
		String fileName = serviceUtil.readFile(null).get(1);
		List<String> fileContent = serviceUtil.readFile(fileName);
		List<ServiceHealth> list = new ArrayList<>();
		try {
			for (String line : fileContent) {
				if (line != null && line.trim().length() > 0) {
					ServiceHealth serviceHealth = new ObjectMapper().readValue(line.getBytes(), ServiceHealth.class);
					if (!serviceHealth.isDeleted())
						list.add(serviceHealth);
				}
			}
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
		}
		routingContext.response().putHeader("content-type", "application/json;charset=utf-8")
				.end(Json.encodePrettily(list));
	}

	private void addOne(RoutingContext routingContext) {
		final ServiceHealth serviceHealth = Json.decodeValue(routingContext.getBodyAsString(), ServiceHealth.class);
		String fileName = serviceUtil.readFile(null).get(1);
		try {
			serviceHealth.setId(String.valueOf(new Random().nextInt()));
			serviceHealth.setStatus("To Be Checked");
			serviceUtil.addServiceData(fileName, new ObjectMapper().writeValueAsString(serviceHealth));
		} catch (JsonProcessingException e) {
			LOGGER.error(e.getMessage(), e);
		}
		routingContext.response().setStatusCode(201).putHeader("content-type", "application/json; charset=utf-8")
				.end(Json.encodePrettily(serviceHealth));
	}

	private void updateOne(RoutingContext routingContext) {
		String name = routingContext.request().getParam("id");
		final ServiceHealth serviceHealth = Json.decodeValue(routingContext.getBodyAsString(), ServiceHealth.class);
		String fileName = serviceUtil.readFile(null).get(1);
		serviceUtil.deleteOrUpdateEntry(fileName, name, serviceHealth, false);
		routingContext.response().setStatusCode(201).putHeader("content-type", "application/json; charset=utf-8")
				.end(Json.encodePrettily(serviceHealth));
	}

	private void deleteOne(RoutingContext routingContext) {
		String name = routingContext.request().getParam("id");
		String fileName = serviceUtil.readFile(null).get(1);
		if (name == null) {
			routingContext.response().setStatusCode(400).end();
		} else {
			serviceUtil.deleteOrUpdateEntry(fileName, name, null, true);
			routingContext.response().setStatusCode(204).end();
		}
	}
}
