package io.vertx.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.vertx.model.ServiceHealth;

public class ServiceUtil {
	private String fileBasePath;
	private String fileForFileName;
	private String fileForServiceData;
	private static final Logger LOGGER = Logger.getLogger(ServiceUtil.class);
	private static final String NEW_LINE = "\n";
	private PropertyUtil propertyUtil;

	public ServiceUtil() {
		propertyUtil = new PropertyUtil();
		fileBasePath = propertyUtil.getProperty("files-base-path");
		fileForFileName = fileBasePath + propertyUtil.getProperty("file-for-fileName");
		fileForServiceData = propertyUtil.getProperty("file-for-serviceData");
	}

	public List<String> readFile(String fileName) {
		FileReader fr = null;
		BufferedReader br = null;
		List<String> list = new ArrayList<>();
		if (fileName == null) {
			fileName = fileForFileName;
		} else {
			fileName = fileBasePath + fileName;
		}
		try {
			File f1 = new File(fileName);
			fr = new FileReader(f1);
			br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				list.add(line);
			}
			return list;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		} finally {
			try {
				if (fr != null)
					fr.close();
				if (br != null)
					br.close();
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
		return Collections.EMPTY_LIST;
	}

	public void addServiceData(String fileName, String json) {
		FileOutputStream fileOut = null;
		try {
			fileOut = new FileOutputStream(fileBasePath + fileName, true);
			fileOut.write((json + "\n").getBytes());
			LOGGER.info("added ServiceData: " + json);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		} finally {
			try {
				if (fileOut != null)
					fileOut.close();
			} catch (IOException e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
	}

	public String updateFileName(String latestFileName) {
		FileReader fr = null;
		BufferedReader br = null;
		FileOutputStream fileOut = null;
		try {
			File f1 = new File(fileForFileName);
			fr = new FileReader(f1);
			br = new BufferedReader(fr);
			String oldFile = br.readLine();
			String newFile = br.readLine();
			LOGGER.info("oldFile: " + oldFile + "newFile: " + newFile);
			if (latestFileName != null) {
				newFile += NEW_LINE + latestFileName;
				fileOut = new FileOutputStream(fileForFileName);
				fileOut.write(newFile.getBytes());
			} else {
				return newFile;
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		} finally {
			try {
				if (fr != null)
					fr.close();
				if (br != null)
					br.close();
				if (latestFileName != null && fileOut != null)
					fileOut.close();
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
		return null;
	}

	public void updateServiceData(String latestFileName, List<String> lines) {
		FileOutputStream fileOut = null;
		try {
			fileOut = new FileOutputStream(fileBasePath + latestFileName);
			for (String line : lines) {
				fileOut.write((line + "\n").getBytes());
			}
			LOGGER.info("File wrote: " + latestFileName);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		} finally {
			try {
				if (fileOut != null)
					fileOut.close();
			} catch (IOException e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
	}

	public void deleteOrUpdateEntry(String fileName, String id, ServiceHealth serviceHealth, boolean isDelete) {
		ObjectMapper objMap = new ObjectMapper();
		String line = null;
		FileReader fr = null;
		BufferedReader br = null;
		List<String> lines = new ArrayList<>();
		String latestFileName = fileForServiceData + new Date().getTime();
		fileName = fileBasePath + fileName;
		try {
			File file = new File(fileName);
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			while ((line = br.readLine()) != null) {
				ServiceHealth service = objMap.readValue(line, ServiceHealth.class);
				if (id.equals(service.getId())) {
					if (isDelete) {
						service.setDeleted(true);
					} else {
						service.setName(serviceHealth.getName());
						service.setUrl(serviceHealth.getUrl());
					}
				}
				lines.add(objMap.writeValueAsString(service));
			}
			LOGGER.info("New Lines: " + lines);
			updateServiceData(latestFileName, lines);
			updateFileName(latestFileName);
			deleteOldFile(fileName);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		} finally {
			try {
				fr.close();
				br.close();
			} catch (IOException e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
	}

	private void deleteOldFile(String fileName) {
		File file = new File(fileName);
		if (file.delete()) {
			LOGGER.debug(fileName + " deleted successfully");
		} else {
			System.out.println("Failed to delete the file");
			LOGGER.debug(fileName + " failed to delete");
		}
	}
}
